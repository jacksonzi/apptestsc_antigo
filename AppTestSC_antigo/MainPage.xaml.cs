﻿using Lumia.Sense;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace AppTestSC_antigo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private IActivityMonitor _activityMonitor = null;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;


            //manage windows status versus sensorcore api
            Window.Current.VisibilityChanged += async (sender, args) =>
            {
                await CallSensorCoreApiAsync(async () =>
                {
                    if (!args.Visible)
                    {
                        // Application put to background, deactivate sensor and unregister change observer
                        if (_activityMonitor != null)
                        {
                            _activityMonitor.Enabled = true;
                            _activityMonitor.ReadingChanged -= activityMonitor_ReadingChanged;
                            await _activityMonitor.DeactivateAsync();
                        }
                    }
                    else
                    {
                        // Make sure all necessary settings are enabled in order to run SensorCore
                        await ValidateSettingsAsync();
                        // Make sure sensor is activated
                        if (_activityMonitor == null)
                        {
                            await InitializeSensorAsync();
                        }
                        else
                        {
                            await _activityMonitor.ActivateAsync();
                        }

                        // Enable change observer
                        _activityMonitor.ReadingChanged += activityMonitor_ReadingChanged;
                        _activityMonitor.Enabled = true;

                        // Update screen
                        //await UpdateSummaryAsync();
                    }
                });
            };
        }

        /// <summary>
        /// Initializes activity monitor sensor
        /// </summary>
        /// <returns>Asynchronous task</returns>
        private async Task InitializeSensorAsync()
        {
            await CallSensorCoreApiAsync(async () => { _activityMonitor = await ActivityMonitor.GetDefaultAsync(); });

            if (_activityMonitor == null)
            {
                // Nothing to do if we cannot use the API
                Application.Current.Exit();
            }
        }

        /// <summary>
        /// Makes sure necessary settings are enabled in order to use SensorCore
        /// </summary>
        /// <returns>Asynchronous task</returns>
        private async Task ValidateSettingsAsync()
        {
            if (!(await ActivityMonitor.IsSupportedAsync()))
            {
                MessageDialog dlg = new MessageDialog("not supported");
                await dlg.ShowAsync();
                Application.Current.Exit();
            }
            else
            {
                uint apiSet = await SenseHelper.GetSupportedApiSetAsync();
                MotionDataSettings settings = await SenseHelper.GetSettingsAsync();
                if (settings.Version < 2)
                {
                    // Device which has old Motion data settings which requires system location and Motion data be enabled in order to access
                    // ActivityMonitor.
                    if (!settings.LocationEnabled)
                    {
                        MessageDialog dlg = new MessageDialog("In order to recognize activities you need to enable location in system settings. Do you want to open settings now? If not, application will exit.", "Information");
                        dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchLocationSettingsAsync())));
                        dlg.Commands.Add(new UICommand("No", new UICommandInvokedHandler((cmd) => { Application.Current.Exit(); })));
                        await dlg.ShowAsync();
                    }
                    else if (!settings.PlacesVisited)
                    {
                        MessageDialog dlg = new MessageDialog("In order to recognize activities you need to enable Motion data in Motion data settings. Do you want to open settings now? If not, application will exit.", "Information");
                        dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchSenseSettingsAsync())));
                        dlg.Commands.Add(new UICommand("No", new UICommandInvokedHandler((cmd) => { Application.Current.Exit(); })));
                        await dlg.ShowAsync();
                    }
                }
                else if (apiSet >= 3)
                {
                    if (!settings.LocationEnabled)
                    {
                        MessageDialog dlg = new MessageDialog("In order to recognize biking you need to enable location in system settings. Do you want to open settings now?", "Helpful tip");
                        dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchLocationSettingsAsync())));
                        dlg.Commands.Add(new UICommand("No"));
                        await dlg.ShowAsync();
                    }
                    else if (settings.DataQuality == DataCollectionQuality.Basic)
                    {
                        MessageDialog dlg = new MessageDialog("In order to recognize biking you need to enable detailed data collection in Motion data settings. Do you want to open settings now?", "Helpful tip");
                        dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchSenseSettingsAsync())));
                        dlg.Commands.Add(new UICommand("No"));
                        await dlg.ShowAsync();
                    }
                }
            }
        }


        /// <summary>
        /// Performs asynchronous Sense SDK operation and handles any exceptions
        /// </summary>
        /// <param name="action">The function delegate to execute asynchronously when one task in the tasks completes</param>
        /// <returns><c>true</c> if call was successful, <c>false</c> otherwise</returns>
        private async Task<bool> CallSensorCoreApiAsync(Func<Task> action)
        {
            Exception failure = null;
            try
            {
                await action();
            }
            catch (Exception e)
            {
                failure = e;
                Debug.WriteLine("Failure:" + e.Message);
            }
            if (failure != null)
            {
                try
                {
                    MessageDialog dialog = null;
                    switch (SenseHelper.GetSenseError(failure.HResult))
                    {
                        case SenseError.LocationDisabled:
                            {
                                dialog = new MessageDialog("In order to recognize activities you need to enable location in system settings. Do you want to open settings now? If not, application will exit.", "Information");
                                dialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchLocationSettingsAsync())));
                                dialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler((cmd) => { Application.Current.Exit(); })));
                                await dialog.ShowAsync();
                                new System.Threading.ManualResetEvent(false).WaitOne(500);
                                return false;
                            }
                        case SenseError.SenseDisabled:
                            {
                                dialog = new MessageDialog("In order to recognize activities you need to enable Motion data in Motion data settings. Do you want to open settings now? If not, application will exit.", "Information");
                                dialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(async (cmd) => await SenseHelper.LaunchSenseSettingsAsync())));
                                dialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler((cmd) => { Application.Current.Exit(); })));
                                await dialog.ShowAsync();
                                return false;
                            }
                        default:
                            dialog = new MessageDialog("Failure: " + SenseHelper.GetSenseError(failure.HResult), "");
                            await dialog.ShowAsync();
                            return false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Failed to handle failure. Message:" + ex.Message);
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        async protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                // Make sure all necessary settings are enabled in order to run SensorCore
                await ValidateSettingsAsync();
                // Make sure sensor is activated
                if (_activityMonitor == null)
                {
                    await InitializeSensorAsync();
                }
                else
                {
                    await _activityMonitor.ActivateAsync();
                }

                // Register change observer
                _activityMonitor.ReadingChanged += activityMonitor_ReadingChanged;
                _activityMonitor.Enabled = true;
                // Update screen
                //await UpdateSummaryAsync();
            }
        }

        /// <summary>
        /// Called when navigating from this page
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected async override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (_activityMonitor != null)
            {
                _activityMonitor.Enabled = false;
                _activityMonitor.ReadingChanged -= activityMonitor_ReadingChanged;
                await _activityMonitor.DeactivateAsync();
            }
        }


        /// <summary>
        /// Called when activity changes
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="args">Event arguments</param>
        private async void activityMonitor_ReadingChanged(IActivityMonitor sender, ActivityMonitorReading args)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Activity activity = args.Mode;
                txtStatus.Text = "status: " + activity.ToString();

                switch (activity)
                {
                    case Activity.Stationary:
                        {
                            txtTeste.FontSize = 20;
                            break;
                        }
                    case Activity.Idle:
                        {
                            txtTeste.FontSize = 20;
                            break;
                        }
                    case Activity.Moving:
                        {
                            txtTeste.FontSize = 25;
                            break;
                        }
                    case Activity.Walking:
                        {
                            txtTeste.FontSize = 30;
                            break;
                        }
                    case Activity.Running:
                        {
                            txtTeste.FontSize = 35;
                            break;
                        }
                }
            });
        }

    }
}
